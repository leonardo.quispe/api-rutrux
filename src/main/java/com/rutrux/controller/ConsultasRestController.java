package com.rutrux.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rutrux.model.Consultas;
import com.rutrux.service.api.ConsultasServiceAPI;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin("*")
public class ConsultasRestController {
	
	@Autowired
	private ConsultasServiceAPI consultasServiceAPI;

	@GetMapping(value = "/all")
	public List<Consultas> getAll() {
		return consultasServiceAPI.getAll();
	}
	
	@GetMapping(value = "/find/{id}")
	public Consultas find(@PathVariable Long id) {
		return consultasServiceAPI.get(id);
	}

	@PostMapping(value = "/save")
	public ResponseEntity<Consultas> save(@RequestBody Consultas consultas) {
		Consultas obj = consultasServiceAPI.save(consultas);
		return new ResponseEntity<Consultas>(obj, HttpStatus.OK);
	}

	@GetMapping(value = "/delete/{id}")
	public ResponseEntity<Consultas> delete(@PathVariable Long id) {
		Consultas consultas = consultasServiceAPI.get(id);
		if (consultas != null) {
			consultasServiceAPI.delete(id);
		} else {
			return new ResponseEntity<Consultas>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Consultas>(consultas, HttpStatus.OK);
	}

}
