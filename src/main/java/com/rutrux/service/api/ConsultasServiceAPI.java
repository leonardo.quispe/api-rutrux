package com.rutrux.service.api;

import com.rutrux.model.Consultas;
import com.rutrux.commons.GenericServiceAPI;

public interface ConsultasServiceAPI extends GenericServiceAPI<Consultas, Long>  {

}
