package com.rutrux.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.rutrux.commons.GenericServiceImpl;
import com.rutrux.dao.api.ConsultasDaoAPI;
import com.rutrux.model.Consultas;
import com.rutrux.service.api.ConsultasServiceAPI;


@Service
public class ConsultasServiceImpl extends GenericServiceImpl<Consultas, Long> implements ConsultasServiceAPI {
	@Autowired
	private ConsultasDaoAPI consultasDaoAPI;
	
	@Override
	public CrudRepository<Consultas, Long> getDao() {
		return consultasDaoAPI;
	}

}
