package com.rutrux.dao.api;

import org.springframework.data.repository.CrudRepository;

import com.rutrux.model.Consultas;

public interface ConsultasDaoAPI extends CrudRepository<Consultas, Long> {

}
